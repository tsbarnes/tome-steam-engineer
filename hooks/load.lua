local DamageType = require "engine.DamageType"
local class = require "engine.class"
local Birther = require "engine.Birther"
local ActorTalents = require "engine.interface.ActorTalents"
local ActorTemporaryEffects = require "engine.interface.ActorTemporaryEffects"

class:bindHook("ToME:load", function(self, data)
	ActorTalents:loadDefinition("/data-steam-engineer/talents/steamtech/steamtech.lua")
	Birther:loadDefinition("/data-steam-engineer/birth/classes/steam-engineerclass.lua")
end)
