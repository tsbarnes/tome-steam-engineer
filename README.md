Powered by steam and ready for anything, the Steam Engineer combines melee
and ranged prowess with technological know-how to deadly effect.

Steam Engineers create deadly robotics to enhance their lethality in combat,
as well as dual-wielding guns and saws with their mechanically enhanced arms.

Their most important stats are: Cunning, Strength, and Dexterity

#### Stat modifiers:
* +3 Strength, +3 Dexterity, +0 Constitution
* +0 Magic, +0 Willpower, +3 Cunning

**Life per level:** *2*
