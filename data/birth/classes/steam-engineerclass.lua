-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2017 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Thea Barnes
-- thea.skye.barnes@gmail.com

local Particles = require "engine.Particles"
getBirthDescriptor("class", "Tinker").descriptor_choices.subclass["Steam Engineer"] = "allow"

newBirthDescriptor{
	type = "subclass",
	name = "Steam Engineer",
	desc = {
		"Powered by steam and ready for anything, the Steam Engineer",
		"combines melee and ranged prowess with technological",
		"know-how to deadly effect.",
		"",
		"Steam Engineers create deadly robotics to enhance their",
		"lethality in combat, as well as dual-wielding guns and",
		"saws with their mechanically enhanced arms.",
		"",
		"Their most important stats are: Cunning, Strength, and Dexterity",
		"#GOLD#Stat modifiers:",
		"#LIGHT_BLUE# * +3 Strength, +3 Dexterity, +0 Constitution",
		"#LIGHT_BLUE# * +0 Magic, +0 Willpower, +3 Cunning",
		"#GOLD#Life per level:#LIGHT_BLUE# 2"
	},
	power_source = {steam=true},
	stats = { dex=3, str=3, cun=3 },
	talents_types = {
		-- Class Skills
		["steamtech/mechanized-body"]={true, 0.3},
		["steamtech/butchery"]={true, 0.3},
		["steamtech/battle-machinery"]={true, 0.3},
		["steamtech/automated-butchery"]={false, 0.3},
		["steamtech/gunner-training"]={true, 0.3},
		["steamtech/automation"]={false, 0.3},

		-- Generic Skills
		["steamtech/chemistry"]={true, 0.2},
		["steamtech/physics"]={true, 0.2},
		["steamtech/blacksmith"]={true, 0.2},
		["steamtech/engineering"]={false, 0.1},
		["technique/combat-training"]={true, 0.3},
	},
	talents = {
		[ActorTalents.T_SMITH] = 1,
		[ActorTalents.T_THERAPEUTICS] = 1,
		[ActorTalents.T_SHOOT] = 1,
		[ActorTalents.T_WEAPON_COMBAT] = 1,
		[ActorTalents.T_STEAMGUN_MASTERY] = 1,
		[ActorTalents.T_STEAMSAW_MASTERY] = 1,
	},
	copy = {
		max_life = 90,
		resolvers.equip{ id=true,
			{type="weapon", subtype="steamgun", name="iron steamgun", base_list="mod.class.Object:/data-orcs/general/objects/steamgun.lua", autoreq=true, ego_chance=-1000},
			{type="weapon", subtype="steamgun", name="iron steamgun", base_list="mod.class.Object:/data-orcs/general/objects/steamgun.lua", autoreq=true, ego_chance=-1000},
			{type="ammo", subtype="shot", name="pouch of iron shots", autoreq=true, ego_chance=-1000},
			{type="armor", subtype="head", name="rough leather hat", autoreq=true, ego_chance=-1000, ego_chance=-1000},
			{type="armor", subtype="light", name="rough leather armour", autoreq=true, ego_chance=-1000, ego_chance=-1000},
			{type="armor", subtype="cloak", name="kruk cloak", base_list="mod.class.Object:/data-orcs/general/objects/special-misc.lua", autoreq=true, ego_chance=-1000, ego_chance=-1000},
		},
		resolvers.inventorybirth{ id=true, inven="QS_MAINHAND",
			{type="weapon", subtype="steamsaw", name="iron steamsaw", base_list="mod.class.Object:/data-orcs/general/objects/steamsaw.lua", autoreq=true, ego_chance=-1000},
		},
		resolvers.inventorybirth{ id=true, inven="QS_OFFHAND",
			{type="weapon", subtype="steamsaw", name="iron steamsaw", base_list="mod.class.Object:/data-orcs/general/objects/steamsaw.lua", autoreq=true, ego_chance=-1000},	
		},
		resolvers.attachtinkerbirth{ id=true,
			{defined="TINKER_ROCKET_BOOTS1"},
			{defined="TINKER_FOCUS_LEN1"},
		},
		resolvers.learn_schematic"HEALING_SALVE",
		resolvers.learn_schematic"ROCKET_BOOTS",
		resolvers.learn_schematic"FOCUS_LEN",
		resolvers.generic(function(e)
			e.auto_shoot_talent = e.T_SHOOT
		end),
	},
	copy_add = {
		life_rating = 2,
	},
}
