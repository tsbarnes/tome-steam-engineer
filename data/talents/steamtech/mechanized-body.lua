-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2017 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Thea Barnes
-- thea.skye.barnes@gmail.com

  newTalent{
    name = "Armed and Dangerous",
    type = {"steamtech/mechanized-body", 1},
    require = cuns_req1,
    points = 5,
    mode = "passive",
    critpower = function(self, t) return self:combatTalentScale(t, 7.5, 20, 0.1) end,
    passives = function(self, t, p)
      self:talentTemporaryValue(p, "combat_critical_power", t.critpower(self, t))
      self:talentTemporaryValue(p, "inc_stats", {
        [self.STAT_DEX] = math.floor(2 * self:getTalentLevel(t)),
        [self.STAT_CUN] = math.floor(2 * self:getTalentLevel(t)),
      })
    end,
    info = function(self, t)
      local statboost = math.floor(2 * self:getTalentLevel(t))
      local power = t.critpower(self, t)
      return ([[You have replaced your forearms and hands with mechanized steampowered robotics,
and have enhanced your nervous system with a constant adrenline injection.
Increases Dexterity and Cunning by %d, and critical hits now do %0.1f%% more damage.]]):
      format(statboost, power)
    end,
  }
  
  newTalent{
    name = "Metal Plating",
    type = {"steamtech/mechanized-body", 2},
    require = cuns_req2,
    points = 5,
    mode = "passive",
    armor = function(self, t)
      return math.floor(self:combatTalentScale(t, 5, 30))
    end,
    hardiness = function(self, t)
      return math.floor(self:combatTalentScale(t, 5, 50))
    end,
    passives = function(self, t, p)
      self:talentTemporaryValue(p, "combat_armor", t.armor(self, t))
      self:talentTemporaryValue(p, "combat_armor_hardiness", t.hardiness(self, t))
    end,
    info = function(self, t)
      local armor = t.armor(self, t)
      local hardiness = t.hardiness(self, t)
      return ([[You embed metal plates in your skin.
  Increases armor by %d and armor hardiness by %d%%.]]):
      format(armor, hardiness)
    end,
  }
  
  newTalent{
    name = "Faraday Cage",
    type = {"steamtech/mechanized-body", 3},
    require = cuns_req3,
    points = 5,
    mode = "passive",
    savemod = function(self, t)
      return math.floor(self:combatTalentScale(t, 5, 50))
    end,
    passives = function(self, t, p)
      self:talentTemporaryValue(p, "combat_mentalresist", t.savemod(self, t))
      self:talentTemporaryValue(p, "combat_spellresist", t.savemod(self, t))
    end,
    info = function(self, t)
      local save = t.savemod(self, t)
      return ([[You construct a faraday cage out of the metal plating in your body, helping block out mental and spell effects.
Increases your mental save and spell save by %d.]]):
      format(save)
    end,
  }
  
  newTalent{
    name = "More Machine",
    type = {"steamtech/mechanized-body", 4},
    require = cuns_req4,
    points = 5,
    mode = "passive",
    getStamRecover = function(self, t) return self:combatTalentScale(t, 0.6, 2.5, 0.75) end,
    getRegen = function(self, t) return self:combatTalentScale(t, 3, 7.5, 0.75) end,
    passives = function(self, t, p)
      self:talentTemporaryValue(p, "stamina_regen", t.getStamRecover(self, t))
      self:talentTemporaryValue(p, "life_regen", t.getRegen(self, t))
      self:talentTemporaryValue(p, "sleep_immune", 1)
    end,
    info = function(self, t)
      local stamregen = t.getStamRecover(self, t)
      local liferegen = t.getRegen(self, t)
      return ([[You are more machine than living creature now, and your machine parts are tireless.
Increases life regen by %0.2f and stamina regen by %0.2f.
Also grants you 100%% sleep immunity.]]):
      format(liferegen, stamregen)
    end,
  }
