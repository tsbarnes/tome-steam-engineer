-- Steam Engineer
-- steam-engineer/init.lua

long_name = "Steam Engineer"
short_name = "steam-engineer" -- Determines the name of your addon's file.
for_module = "tome"
version = {1,5,10}
addon_version = {1,0,0}
weight = 100 -- The lower this value, the sooner your addon will load compared to other addons.
author = {'thea.skye.barnes@gmail.com'}
homepage = ''
description = [[Adds a Steam Engineer class, with their powerful Steam Golem
]] -- the [[ ]] things are like quote marks that can span multiple lines
tags = {'tinker', 'engineer', 'golem'} -- tags MUST immediately follow description

overload = true
superload = true
data = true
hooks = true